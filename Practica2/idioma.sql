insert into idioma (id_idioma, nombre, ultima_actualizacion) values (1, 'Irish Gaelic', '8/3/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (2, 'Maltese', '12/19/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (3, 'Croatian', '3/12/2024');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (4, 'Burmese', '7/23/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (5, 'Catalan', '5/6/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (6, 'Chinese', '8/2/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (7, 'Tsonga', '10/3/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (8, 'Italian', '1/29/2024');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (9, 'Tswana', '10/21/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (10, 'Persian', '6/16/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (11, 'Latvian', '4/1/2024');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (12, 'Polish', '3/17/2024');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (13, 'Chinese', '7/25/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (14, 'Sotho', '3/7/2024');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (15, 'Hungarian', '7/21/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (16, 'Portuguese', '4/2/2024');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (17, 'Bislama', '3/13/2024');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (18, 'Kannada', '10/4/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (19, 'Azeri', '2/6/2024');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (20, 'Swedish', '8/15/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (21, 'Tamil', '2/5/2024');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (22, 'Somali', '5/11/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (23, 'Marathi', '5/31/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (24, 'Irish Gaelic', '2/2/2024');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (25, 'Indonesian', '3/29/2024');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (26, 'Spanish', '3/31/2024');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (27, 'Greek', '1/13/2024');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (28, 'Northern Sotho', '4/4/2024');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (29, 'Bosnian', '12/24/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (30, 'Dari', '4/3/2024');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (31, 'Aymara', '4/8/2024');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (32, 'Finnish', '7/7/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (33, 'Khmer', '11/6/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (34, 'Polish', '9/12/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (35, 'Khmer', '7/12/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (36, 'Bislama', '12/19/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (37, 'Azeri', '7/25/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (38, 'Norwegian', '5/9/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (39, 'Sotho', '6/7/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (40, 'West Frisian', '2/3/2024');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (41, 'Bislama', '1/22/2024');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (42, 'Hiri Motu', '12/4/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (43, 'Hindi', '6/11/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (44, 'Japanese', '4/11/2024');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (45, 'Pashto', '8/8/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (46, 'Bislama', '7/6/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (47, 'Swati', '5/16/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (48, 'Georgian', '8/17/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (49, 'Kashmiri', '4/27/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (50, 'Tamil', '5/17/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (51, 'Latvian', '9/10/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (52, 'Montenegrin', '10/30/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (53, 'New Zealand Sign Language', '9/16/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (54, 'Swahili', '9/3/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (55, 'Armenian', '3/2/2024');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (56, 'Tok Pisin', '3/29/2024');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (57, 'Tsonga', '12/11/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (58, 'Bislama', '7/12/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (59, 'Moldovan', '9/3/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (60, 'Assamese', '3/9/2024');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (61, 'Tok Pisin', '5/4/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (62, 'Catalan', '2/24/2024');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (63, 'Hebrew', '4/29/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (64, 'Guaraní', '2/6/2024');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (65, 'Tswana', '7/10/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (66, 'Amharic', '1/7/2024');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (67, 'Burmese', '2/10/2024');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (68, 'Moldovan', '7/17/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (69, 'Latvian', '3/28/2024');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (70, 'Yiddish', '11/9/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (71, 'Italian', '6/25/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (72, 'Kannada', '10/9/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (73, 'Azeri', '4/21/2024');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (74, 'Croatian', '4/1/2024');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (75, 'Ndebele', '12/7/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (76, 'Catalan', '2/3/2024');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (77, 'Hindi', '1/18/2024');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (78, 'Mongolian', '5/8/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (79, 'Maltese', '8/22/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (80, 'Sotho', '4/22/2024');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (81, 'Catalan', '2/25/2024');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (82, 'Indonesian', '4/25/2024');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (83, 'Greek', '5/11/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (84, 'Montenegrin', '1/18/2024');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (85, 'Tamil', '6/19/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (86, 'Polish', '3/10/2024');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (87, 'French', '11/3/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (88, 'Assamese', '5/2/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (89, 'Romanian', '4/24/2024');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (90, 'Aymara', '2/23/2024');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (91, 'Dutch', '12/7/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (92, 'West Frisian', '12/30/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (93, 'Estonian', '5/21/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (94, 'Bosnian', '10/11/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (95, 'Tsonga', '4/9/2024');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (96, 'Tajik', '5/25/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (97, 'West Frisian', '10/18/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (98, 'Kyrgyz', '3/18/2024');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (99, 'Luxembourgish', '5/28/2023');
insert into idioma (id_idioma, nombre, ultima_actualizacion) values (100, 'Quechua', '12/17/2023');
