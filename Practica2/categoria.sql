insert into categoria (id_categoria, nombre, ultima_actualizacion) values (1, 'Drama', '4/25/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (2, 'Drama', '6/18/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (3, 'Drama|Horror|Romance', '2/2/2024');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (4, 'Children|Comedy|Fantasy', '6/26/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (5, 'Horror', '6/28/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (6, 'Documentary', '7/12/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (7, 'Comedy|Drama|Romance', '2/26/2024');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (8, 'Action|Comedy|Drama', '3/10/2024');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (9, 'Drama', '1/17/2024');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (10, 'Documentary', '3/12/2024');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (11, 'Drama', '5/11/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (12, 'Drama', '2/7/2024');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (13, 'Crime|Drama', '6/29/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (14, 'Crime|Thriller', '2/3/2024');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (15, 'Comedy|Musical|Romance', '4/8/2024');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (16, 'Drama|Romance', '3/5/2024');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (17, 'Adventure|Animation|Comedy', '1/15/2024');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (18, 'Action|Adventure', '8/11/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (19, 'Comedy|Drama', '11/26/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (20, 'Comedy|Drama', '11/17/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (21, 'Drama', '6/2/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (22, 'Comedy', '6/10/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (23, 'Documentary', '12/3/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (24, 'Adventure|Comedy|Romance', '3/3/2024');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (25, 'Horror', '3/12/2024');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (26, 'Horror|Mystery|Thriller', '5/23/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (27, 'Thriller', '11/6/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (28, 'Drama|Thriller', '11/10/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (29, 'Documentary', '9/13/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (30, 'Drama', '11/24/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (31, 'Fantasy|Musical', '2/29/2024');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (32, 'Comedy', '2/28/2024');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (33, 'Comedy|Drama|Fantasy', '3/25/2024');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (34, 'Documentary', '1/28/2024');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (35, 'Action|Drama|Thriller', '10/11/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (36, 'Drama|War', '3/9/2024');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (37, 'Drama', '2/8/2024');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (38, 'Drama', '11/20/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (39, 'Comedy|Drama', '4/25/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (40, 'Crime|Drama|Romance', '4/5/2024');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (41, 'Documentary|Musical', '8/18/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (42, 'Comedy|Musical', '12/8/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (43, 'Comedy|Romance', '2/15/2024');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (44, 'Drama|Romance|War', '8/4/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (45, 'Comedy', '10/16/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (46, 'Crime|Drama', '11/16/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (47, 'Action|Thriller', '4/27/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (48, 'Comedy|Drama', '1/31/2024');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (49, 'Crime|Drama', '6/19/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (50, 'Action|Comedy|Crime|Thriller', '8/1/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (51, 'Mystery|Thriller', '6/23/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (52, 'Mystery|Thriller', '1/13/2024');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (53, 'Action|Adventure|Thriller', '12/12/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (54, 'Drama', '4/13/2024');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (55, 'Animation|Children|Musical|IMAX', '12/4/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (56, 'Drama', '11/22/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (57, 'Action|Comedy', '7/9/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (58, 'Comedy|Drama|Romance', '7/20/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (59, 'Horror', '10/22/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (60, 'Comedy', '7/28/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (61, 'Comedy|Drama|Romance', '5/13/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (62, 'Comedy', '1/9/2024');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (63, 'Drama|Thriller', '8/25/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (64, 'Adventure|Animation|Children|Comedy|Fantasy', '10/13/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (65, 'Action|Fantasy|Horror', '12/12/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (66, 'Comedy|Romance', '4/27/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (67, 'Comedy', '11/27/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (68, 'Drama', '8/8/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (69, 'Comedy', '3/16/2024');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (70, 'Action', '5/2/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (71, 'Comedy|Romance', '3/5/2024');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (72, 'Western', '10/14/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (73, 'Thriller', '5/12/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (74, 'Drama', '6/12/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (75, 'Drama', '9/5/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (76, 'Drama|Mystery', '9/29/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (77, 'Drama', '6/26/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (78, 'Comedy|Horror', '12/28/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (79, 'Fantasy|Horror|Mystery|War', '5/27/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (80, 'Drama|Romance', '4/7/2024');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (81, 'Drama|Musical', '8/14/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (82, 'Comedy|Musical|Romance', '4/21/2024');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (83, 'War', '8/4/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (84, 'Drama', '1/20/2024');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (85, 'Drama', '3/2/2024');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (86, 'Comedy', '9/17/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (87, 'Drama', '7/25/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (88, 'Horror', '5/22/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (89, 'Comedy|Romance', '4/24/2024');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (90, 'Drama|Romance', '7/31/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (91, 'Documentary', '3/10/2024');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (92, 'Documentary', '11/2/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (93, 'Action|Adventure|Children|Comedy|Fantasy|Sci-Fi', '10/1/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (94, 'Drama|Romance', '4/18/2024');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (95, 'Action|Drama|Sci-Fi|Thriller', '5/31/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (96, 'Action|Horror', '10/16/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (97, 'Comedy', '6/10/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (98, 'Animation', '12/7/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (99, 'Horror|Mystery|Sci-Fi|Thriller', '9/22/2023');
insert into categoria (id_categoria, nombre, ultima_actualizacion) values (100, 'Action|Comedy|Romance|War', '12/6/2023');
