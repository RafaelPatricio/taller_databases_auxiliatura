insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (1, 1, 'Kippie', 'Blennerhassett', 'kblennerhassett0@meetup.com', 1, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (2, 2, 'Elsey', 'Ganter', 'eganter1@wikimedia.org', 2, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (3, 3, 'Sally', 'Veschi', 'sveschi2@feedburner.com', 3, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (4, 4, 'Marni', 'Tewnion', 'mtewnion3@sun.com', 4, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (5, 5, 'Rogerio', 'Delacour', 'rdelacour4@mozilla.org', 5, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (6, 6, 'Sibeal', 'Telford', 'stelford5@vimeo.com', 6, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (7, 7, 'Jasen', 'Stanlick', 'jstanlick6@wisc.edu', 7, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (8, 8, 'Tully', 'Jenicke', 'tjenicke7@desdev.cn', 8, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (9, 9, 'Shawnee', 'Fiddyment', 'sfiddyment8@wired.com', 9, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (10, 10, 'Chad', 'Bilverstone', 'cbilverstone9@webeden.co.uk', 10, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (11, 11, 'Naoma', 'Uppett', 'nuppetta@sbwire.com', 11, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (12, 12, 'Pat', 'Botler', 'pbotlerb@example.com', 12, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (13, 13, 'Giacomo', 'Juhruke', 'gjuhrukec@redcross.org', 13, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (14, 14, 'Waiter', 'Getcliffe', 'wgetcliffed@w3.org', 14, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (15, 15, 'Marrilee', 'Chidler', 'mchidlere@simplemachines.org', 15, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (16, 16, 'Baxy', 'Offener', 'boffenerf@yellowbook.com', 16, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (17, 17, 'Ken', 'Jiricka', 'kjirickag@sourceforge.net', 17, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (18, 18, 'Burt', 'Dicty', 'bdictyh@g.co', 18, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (19, 19, 'Vallie', 'Sineath', 'vsineathi@springer.com', 19, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (20, 20, 'Orland', 'Naseby', 'onasebyj@samsung.com', 20, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (21, 21, 'Aland', 'Masdin', 'amasdink@loc.gov', 21, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (22, 22, 'Thurstan', 'Morehall', 'tmorehalll@senate.gov', 22, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (23, 23, 'Clementine', 'O''Mahoney', 'comahoneym@hud.gov', 23, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (24, 24, 'Garreth', 'Wasling', 'gwaslingn@facebook.com', 24, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (25, 25, 'Katerina', 'Caff', 'kcaffo@naver.com', 25, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (26, 26, 'Ola', 'Millions', 'omillionsp@facebook.com', 26, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (27, 27, 'Fabian', 'Hinkley', 'fhinkleyq@ovh.net', 27, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (28, 28, 'Fleurette', 'Andreopolos', 'fandreopolosr@uol.com.br', 28, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (29, 29, 'Ava', 'Fulger', 'afulgers@xinhuanet.com', 29, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (30, 30, 'Urbanus', 'McLice', 'umclicet@rakuten.co.jp', 30, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (31, 31, 'Lock', 'Gleave', 'lgleaveu@multiply.com', 31, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (32, 32, 'Gregg', 'Stower', 'gstowerv@twitpic.com', 32, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (33, 33, 'Pamela', 'Strapp', 'pstrappw@apache.org', 33, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (34, 34, 'Thorndike', 'Sewards', 'tsewardsx@bing.com', 34, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (35, 35, 'Hailee', 'Ambrosoni', 'hambrosoniy@pen.io', 35, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (36, 36, 'Ettore', 'Conneau', 'econneauz@ask.com', 36, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (37, 37, 'Lewie', 'Espinho', 'lespinho10@gnu.org', 37, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (38, 38, 'Lela', 'Bonnin', 'lbonnin11@wufoo.com', 38, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (39, 39, 'Ettore', 'Korda', 'ekorda12@eepurl.com', 39, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (40, 40, 'Alic', 'Leworthy', 'aleworthy13@admin.ch', 40, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (41, 41, 'Karola', 'Vern', 'kvern14@reuters.com', 41, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (42, 42, 'Gabriel', 'Smeeth', 'gsmeeth15@360.cn', 42, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (43, 43, 'Thorn', 'Noseworthy', 'tnoseworthy16@dailymotion.com', 43, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (44, 44, 'Abba', 'Wye', 'awye17@nasa.gov', 44, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (45, 45, 'Batsheva', 'Colombier', 'bcolombier18@geocities.com', 45, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (46, 46, 'Debora', 'Seymark', 'dseymark19@hao123.com', 46, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (47, 47, 'Adriano', 'Quarless', 'aquarless1a@europa.eu', 47, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (48, 48, 'Tobye', 'Rosenzveig', 'trosenzveig1b@linkedin.com', 48, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (49, 49, 'Clovis', 'Forestall', 'cforestall1c@over-blog.com', 49, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (50, 50, 'Catlee', 'Tocqueville', 'ctocqueville1d@clickbank.net', 50, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (51, 51, 'Paxton', 'Baukham', 'pbaukham1e@statcounter.com', 51, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (52, 52, 'Allx', 'Eastam', 'aeastam1f@xing.com', 52, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (53, 53, 'Yardley', 'Ponten', 'yponten1g@jigsy.com', 53, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (54, 54, 'Blondy', 'Dufaire', 'bdufaire1h@alibaba.com', 54, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (55, 55, 'Humfrey', 'Flohard', 'hflohard1i@360.cn', 55, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (56, 56, 'Read', 'Devenny', 'rdevenny1j@macromedia.com', 56, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (57, 57, 'Gabrielle', 'Scarrisbrick', 'gscarrisbrick1k@diigo.com', 57, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (58, 58, 'Clay', 'Everwin', 'ceverwin1l@themeforest.net', 58, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (59, 59, 'Gunilla', 'Redbourn', 'gredbourn1m@msu.edu', 59, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (60, 60, 'Karyn', 'Claringbold', 'kclaringbold1n@mac.com', 60, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (61, 61, 'Roda', 'Sedgman', 'rsedgman1o@w3.org', 61, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (62, 62, 'Shaylynn', 'MacDearmid', 'smacdearmid1p@jimdo.com', 62, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (63, 63, 'Zorine', 'McQuilty', 'zmcquilty1q@jalbum.net', 63, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (64, 64, 'Chrisse', 'Linzee', 'clinzee1r@imageshack.us', 64, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (65, 65, 'Federico', 'Smerdon', 'fsmerdon1s@unc.edu', 65, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (66, 66, 'Drusilla', 'Crisp', 'dcrisp1t@spiegel.de', 66, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (67, 67, 'Marius', 'Spenceley', 'mspenceley1u@ycombinator.com', 67, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (68, 68, 'Phillip', 'Rennenbach', 'prennenbach1v@ca.gov', 68, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (69, 69, 'Bent', 'Colcomb', 'bcolcomb1w@statcounter.com', 69, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (70, 70, 'Kelby', 'Furmenger', 'kfurmenger1x@posterous.com', 70, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (71, 71, 'Gert', 'Wainwright', 'gwainwright1y@gov.uk', 71, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (72, 72, 'Yvon', 'Dominichelli', 'ydominichelli1z@taobao.com', 72, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (73, 73, 'Dunstan', 'Jiricka', 'djiricka20@tinyurl.com', 73, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (74, 74, 'Granny', 'Rockwill', 'grockwill21@sakura.ne.jp', 74, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (75, 75, 'Susi', 'Jiruca', 'sjiruca22@flavors.me', 75, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (76, 76, 'Kori', 'Sandars', 'ksandars23@msn.com', 76, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (77, 77, 'Consolata', 'Cluely', 'ccluely24@photobucket.com', 77, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (78, 78, 'Cleveland', 'Grishakin', 'cgrishakin25@disqus.com', 78, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (79, 79, 'Danya', 'Soulsby', 'dsoulsby26@arizona.edu', 79, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (80, 80, 'Esdras', 'Bosche', 'ebosche27@google.co.uk', 80, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (81, 81, 'Gaye', 'Hafner', 'ghafner28@tamu.edu', 81, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (82, 82, 'Cornie', 'Anthiftle', 'canthiftle29@artisteer.com', 82, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (83, 83, 'Adiana', 'Abate', 'aabate2a@nyu.edu', 83, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (84, 84, 'Nana', 'Hartopp', 'nhartopp2b@theglobeandmail.com', 84, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (85, 85, 'Rosetta', 'Tippings', 'rtippings2c@wiley.com', 85, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (86, 86, 'Adam', 'Woolcocks', 'awoolcocks2d@java.com', 86, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (87, 87, 'Redd', 'Lunbech', 'rlunbech2e@bloglines.com', 87, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (88, 88, 'Sergio', 'Cromett', 'scromett2f@so-net.ne.jp', 88, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (89, 89, 'Floria', 'Woodyeare', 'fwoodyeare2g@ucoz.com', 89, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (90, 90, 'Patrizius', 'Ludgate', 'pludgate2h@yolasite.com', 90, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (91, 91, 'Yorker', 'Lambdon', 'ylambdon2i@e-recht24.de', 91, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (92, 92, 'Boone', 'Moresby', 'bmoresby2j@pbs.org', 92, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (93, 93, 'Allsun', 'Wayt', 'awayt2k@earthlink.net', 93, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (94, 94, 'Mellie', 'Anderer', 'manderer2l@kickstarter.com', 94, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (95, 95, 'Gregg', 'Coode', 'gcoode2m@va.gov', 95, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (96, 96, 'Rainer', 'Spieght', 'rspieght2n@google.com.hk', 96, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (97, 97, 'Odey', 'Morriarty', 'omorriarty2o@posterous.com', 97, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (98, 98, 'Delcina', 'Spilisy', 'dspilisy2p@vk.com', 98, false);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (99, 99, 'Fina', 'Adamovsky', 'fadamovsky2q@google.com.br', 99, true);
insert into cliente (id_cliente, id_tienda, nombre, apellido, correo_electronico, id_direccion, activo) values (100, 100, 'Ned', 'Joselevitch', 'njoselevitch2r@so-net.ne.jp', 100, false);
