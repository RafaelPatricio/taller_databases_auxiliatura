-- Tabla de cliente
CREATE TABLE cliente (
    id_cliente INT PRIMARY KEY,
    id_tienda INT,
    nombre VARCHAR(50),
    apellido VARCHAR(50),
    correo_electronico VARCHAR(100),
    id_direccion INT,
    activo BOOLEAN,
    FOREIGN KEY (id_tienda) REFERENCES tienda(id_tienda),
    FOREIGN KEY (id_direccion) REFERENCES direccion(id_direccion)
);

-- Tabla de Actores
CREATE TABLE actor (
    id_actor INT PRIMARY KEY,
    nombre VARCHAR(50),
    apellido VARCHAR(50),
    ultima_actualizacion TIMESTAMP
);

-- Tabla de Categorías
CREATE TABLE categoria (
    id_categoria INT PRIMARY KEY,
    nombre VARCHAR(50),
    ultima_actualizacion TIMESTAMP
);

-- Tabla de Películas
CREATE TABLE pelicula (
    id_pelicula INT PRIMARY KEY,
    titulo VARCHAR(255),
    descripcion TEXT,
    ano_estreno INT,
    id_idioma INT,
    id_idioma_original INT,
    duracion_alquiler INT,
    tarifa_alquiler FLOAT,
    duracion INT,
    costo_reemplazo FLOAT,
    clasificacion VARCHAR(10),
    ultima_actualizacion TIMESTAMP,
    caracteristicas_especiales TEXT,
    texto_completo TEXT,
    FOREIGN KEY (id_idioma) REFERENCES idioma(id_idioma),
    FOREIGN KEY (id_idioma_original) REFERENCES idioma(id_idioma)
);

-- Tabla de Actores en Películas
CREATE TABLE actor_pelicula (
    id_actor INT,
    id_pelicula INT,
    ultima_actualizacion TIMESTAMP,
    FOREIGN KEY (id_actor) REFERENCES actor(id_actor),
    FOREIGN KEY (id_pelicula) REFERENCES pelicula(id_pelicula)
);

-- Tabla de Categorías de Películas
CREATE TABLE categoria_pelicula (
    id_pelicula INT,
    id_categoria INT,
    ultima_actualizacion TIMESTAMP,
    FOREIGN KEY (id_pelicula) REFERENCES pelicula(id_pelicula),
    FOREIGN KEY (id_categoria) REFERENCES categoria(id_categoria)
);

-- Tabla de Direcciones
CREATE TABLE direccion (
    id_direccion INT PRIMARY KEY,
    direccion VARCHAR(255),
    direccion2 VARCHAR(255),
    distrito VARCHAR(50),
    id_ciudad INT,
    codigo_postal VARCHAR(10),
    telefono VARCHAR(15),
    ultima_actualizacion TIMESTAMP,
    FOREIGN KEY (id_ciudad) REFERENCES ciudad(id_ciudad)
);

-- Tabla de Ciudades
CREATE TABLE ciudad (
    id_ciudad INT PRIMARY KEY,
    ciudad VARCHAR(100),
    id_pais INT,
    ultima_actualizacion TIMESTAMP
    FOREIGN KEY (id_pais) REFERENCES pais(id_pais)
);

-- Tabla de Países
CREATE TABLE pais (
    id_pais INT PRIMARY KEY,
    pais VARCHAR(100),
    ultima_actualizacion TIMESTAMP
);

-- Tabla de Inventario
    CREATE TABLE inventario (
        id_inventario INT PRIMARY KEY,
        id_pelicula INT,
        id_tienda INT,
        ultima_actualizacion TIMESTAMP,
        FOREIGN KEY (id_pelicula) REFERENCES pelicula(id_pelicula),
        FOREIGN KEY (id_tienda) REFERENCES tienda(id_tienda)
    );

-- Tabla de Idiomas
CREATE TABLE idioma (
    id_idioma INT PRIMARY KEY,
    nombre VARCHAR(50),
    ultima_actualizacion TIMESTAMP
);

-- Tabla de Pagos
CREATE TABLE pago (
    id_pago INT PRIMARY KEY,
    id_cliente INT,
    id_personal INT,
    id_alquiler INT,
    monto FLOAT,
    fecha_pago DATE,
    ultima_actualizacion TIMESTAMP,
    FOREIGN KEY (id_cliente) REFERENCES cliente(id_cliente),
    FOREIGN KEY (id_personal) REFERENCES personal(id_personal),
    FOREIGN KEY (id_alquiler) REFERENCES renta(id_renta)
);

-- Tabla de Rentas
CREATE TABLE renta (
    id_renta INT PRIMARY KEY,
    fecha_renta DATE,
    id_inventario INT,
    id_cliente INT,
    fecha_devolucion DATE,
    id_personal INT,
    ultima_actualizacion TIMESTAMP,
    FOREIGN KEY (id_inventario) REFERENCES inventario(id_inventario),
    FOREIGN KEY (id_cliente) REFERENCES cliente(id_cliente),
    FOREIGN KEY (id_personal) REFERENCES personal(id_personal)
);

-- Tabla de Personal
CREATE TABLE personal (
    id_personal INT PRIMARY KEY,
    nombre VARCHAR(50),
    apellido VARCHAR(50),
    id_direccion INT,
    correo_electronico VARCHAR(100),
    id_tienda INT,
    activo BOOLEAN, 
    nombre_usuario VARCHAR(50),
    contrasena VARCHAR(100),
    ultima_actualizacion TIMESTAMP,
    FOREIGN KEY (id_direccion) REFERENCES direccion(id_direccion),
    FOREIGN KEY (id_tienda) REFERENCES tienda(id_tienda)
);

-- Tabla de Tiendas
CREATE TABLE tienda (
    id_tienda INT PRIMARY KEY,
    id_personal INT,
    id_direccion INT,
    ultima_actualizacion TIMESTAMP,
    FOREIGN KEY (id_direccion) REFERENCES direccion(id_direccion)
);
